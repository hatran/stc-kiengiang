﻿
"use strict";
$(document).ready(function() {
      $(".tstInfo").on("click",function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'info',
            hideAfter: 3000, 
            stack: 6
          });

     });

      $(".tstWarning").on("click",function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'warning',
            hideAfter: 3500, 
            stack: 6
          });

      });

      $(".tstMailDeletedWarning").on("click", function () {
          $.toast({
              heading: 'Cảnh báo, Bạn vừa xóa Email thành công',
              text: 'Bạn có thể kiểm phục hồi thư đã xóa trong thùng rác nếu bạn muốn ngay bây giờ.',
              position: 'top-right',
              loaderBg: '#ff6849',
              icon: 'warning',
              hideAfter: 3500,
              stack: 6
          });

      });
      $(".tstItemDeletedWarning").on("click", function () {
          $.toast({
              heading: 'Cảnh báo xóa thông tin',
              text: 'Bạn vừa xóa thành công mục tin.',
              position: 'top-right',
              loaderBg: '#ff6849',
              icon: 'warning',
              hideAfter: 3500,
              stack: 6
          });

      });
      $(".tstDeleteItem").on("click", function () {
          $.toast({
              heading: 'Cảnh báo xóa thông ti',
              text: 'Bạn vừa xóa thành công mục tin.',
              position: 'top-right',
              loaderBg: '#ff6849',
              icon: 'error',
              hideAfter: 3500

          });

      });
      $(".tstAddNewItemSuccess").on("click", function () {
          $.toast({
              heading: 'Thông báo',
              text: 'Bạn vừa thêm mới thành công mục tin.',
              position: 'top-right',
              loaderBg: '#ff6849',
              icon: 'success',
              hideAfter: 3500,
              stack: 6
          });

      });
      $(".tstSuccess").on("click",function(){
           $.toast({
            heading: 'Welcome to my Elite admin',
            text: 'Use the predefined ones, or specify a custom position object.',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
          });

      });
      $(".tstSendEmailSuccess").on("click", function () {
          $.toast({
              heading: 'Thông báo gửi mail thành công',
              text: 'Chúc mừng bạn đã gửi email thành công, người nhận sẽ nhận được thư của bạn trong giây lát.',
              position: 'top-right',
              loaderBg: '#ff6849',
              icon: 'success',
              hideAfter: 3500,
              stack: 6
          });
    });
    $(".tstVanBanTraLoi").on("click", function () {
        $.toast({
            heading: 'Thông báo trả lời văn bản thành công',
            text: 'Chúc mừng bạn đã trả lời văn bản thành công.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    });
    $(".tstXuLy").on("click", function () {
        $.toast({
            heading: 'Thông báo xử lý văn bản thành công',
            text: 'Chúc mừng bạn đã xử lý văn bản thành công.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    });
    $(".tstLuHS").on("click", function () {
        $.toast({
            heading: 'Thông báo Lưu hồ sơ thành công',
            text: 'Chúc mừng bạn Lưu hồ sơ thành công.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    });
    //$(".tstVanBanTraLoi").on("click", function () {
    //    $.toast({
    //        heading: 'Thông báo trả lời văn bản thành công',
    //        text: 'Chúc mừng bạn đã trả lời văn bản thành công.',
    //        position: 'top-right',
    //        loaderBg: '#ff6849',
    //        icon: 'success',
    //        hideAfter: 3500,
    //        stack: 6
    //    });
    //});
    $(".tstLuuChuyen").on("click", function () {
        $.toast({
            heading: 'Thông báo lưu chuyển thành công',
            text: 'Chúc mừng bạn đã lưu chuyển thành công.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    });
    $(".tstGiaoNhiemVu").on("click", function () {
        $.toast({
            heading: 'Thông báo giao nhiệm vụ thành công',
            text: 'Chúc mừng bạn đã giao nhiệm vụ thành công.',
            position: 'top-right',
            loaderBg: '#ff6849',
            icon: 'success',
            hideAfter: 3500,
            stack: 6
        });
    });
      $(".tstSaveEmailSuccess").on("click", function () {
          $.toast({
              heading: 'Thông báo lưu Email thành công',
              text: 'Bạn đã lưu Email thành công, bạn có thể gửi mail khi nào bạn muốn.',
              position: 'top-right',
              loaderBg: '#ff6849',
              icon: 'success',
              hideAfter: 3500,
              stack: 6
          });
      });
      $(".tstSimple").on("click",function(){
    	  $.toast({
    		  	position: 'top-left',
    		    text: 'This will become the toast message'
    		})
      }); 
      $(".tstArray").on("click",function(){
    	  $.toast({
      	    heading: 'How to contribute?!',
      	    position: 'top-left',
      	    text: [
      	        'Fork the repository', 
      	        'Improve/extend the functionality', 
      	        'Create a pull request'
      	    ],
      	    icon: 'info'
      	})

    });
      
      $(".tstHtml").on("click",function(){
    	  $.toast({
    		    heading: 'Can I add <em>icons</em>?',
    		    position: 'top-left',
    		    text: 'Yes! check this <a href="https://github.com/kamranahmedse/jquery-toast-plugin/commits/master">update</a>.',
    		    hideAfter: false,
    		    icon: 'success'
    		})

    });
      $(".tstSticky").on("click",function(){
    	  $.toast({
    		    text: 'Set the `hideAfter` property to false and the toast will become sticky.',
    		    hideAfter: false
    		})
    }); 
//      Animation
      $(".tstFade").on("click",function(){
    	  $.toast({
    		    text: 'Set the `showHideTransition` property to fade|plain|slide to achieve different transitions',
    		    heading: 'Fade transition',
    		    position: 'top-left',
    		    showHideTransition: 'fade'
    		})
      });
      $(".tstSlide").on("click",function(){
    	  $.toast({
    		    text: 'Set the `showHideTransition` property to fade|plain|slide to achieve different transitions',
    		    heading: 'Slide transition',
    		    position: 'top-left',
    		    showHideTransition: 'slide'
    		})
      });
      $(".tstPlain").on("click",function(){
    	  $.toast({
    		    text: 'Set the `showHideTransition` property to fade|plain|slide to achieve different transitions',
    		    heading: 'Plain transition',
    		    position: 'top-left',
    		    showHideTransition: 'plain'
    		})
      });
//      position

      $(".tstBtmLeft").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Specify the custom position object or use one of the predefined ones',
    		    position: 'bottom-left',
    		    stack: false
    		})
      });
      $(".tstBtmRight").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Specify the custom position object or use one of the predefined ones',
    		    position: 'bottom-right',
    		    stack: false
    		})
      });
      $(".tstBtmCenter").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Use the predefined ones, or specify a custom object',
    		    position: 'bottom-center',
    		    stack: false
    		})
      });
      $(".tstTopLeft").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Use the predefined ones, or specify a custom object',
    		    position: 'top-left',
    		    stack: false
    		})
      });
      $(".tstTopCenter").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Use the predefined ones, or specify a custom position object.',
    		    position: 'top-center',
    		    stack: false
    		})
      });
      $(".tstTopRight").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Use the predefined ones, or specify a custom position object.',
    		    position: 'top-right',
    		    stack: false
    		})
      });
      $(".tstMidCenter").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Use the predefined ones, or specify a custom position object.',
    		    position: 'mid-center',
    		    stack: false
    		})
      });
      $(".tstCustom").on("click",function(){
    	  $.toast().reset('all');
    	  $.toast({
    		    heading: 'Positioning',
    		    text: 'Use the predefined ones, or specify a custom position object.',
    		    position: {
    		        left: 120,
    		        top: 120
    		    },
    		    stack: false
    		})
      });
});
          